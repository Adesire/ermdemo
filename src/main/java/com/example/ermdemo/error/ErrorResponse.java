package com.example.ermdemo.error;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ErrorResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date timestamp;
    private String responseCode;
    private String error;
    private String message;
}
