package com.example.ermdemo.controller;

import com.example.ermdemo.constant.ResponseStatusCode;
import com.example.ermdemo.dto.LevelDto;
import com.example.ermdemo.dto.LineManagerDto;
import com.example.ermdemo.error.ErrorResponse;
import com.example.ermdemo.model.LineManager;
import com.example.ermdemo.response.LevelResponse;
import com.example.ermdemo.response.LineManagerResponse;
import com.example.ermdemo.response.MultiSuccessResponse;
import com.example.ermdemo.response.SingleSuccessResponse;
import com.example.ermdemo.service.LineManagerService;
import com.example.ermdemo.util.UtilityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/lineManagers")
public class LineManagerController {

    @Autowired
    private LineManagerService lineManagerService;

    @PostMapping
    @ApiOperation(value = "Add new Line Manager to the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> addLineManager(@RequestBody LineManagerDto lineManagerDto) {
        LineManagerResponse lineManagerResponse = lineManagerService.addLineManager(lineManagerDto);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"LineManager created successfully", lineManagerResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Get list of Line Managers", response = MultiSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = MultiSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getAllLineManager(){
        List<LineManagerResponse> lineManagers = lineManagerService.findAllLineManagers();
        MultiSuccessResponse successResponse = new MultiSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", lineManagers);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a Line Manager by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getLineManager(@PathVariable Long id){
        LineManagerResponse lineManager = lineManagerService.findLineManager(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", lineManager);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }


    @PutMapping("/{id}")
    @ApiOperation(value = "Update a Line Manager in the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> updateLineManager(@RequestBody LineManagerDto lineManagerDto, @PathVariable Long id){
        LineManagerResponse lineManagerResponse = lineManagerService.updateLineManager(lineManagerDto, id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Line Manager updated successfully", lineManagerResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a LineManager by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> deleteLineManager(@PathVariable Long id) {
        lineManagerService.deleteLineManager(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK, "LineManager successfully deleted", null);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }
}
