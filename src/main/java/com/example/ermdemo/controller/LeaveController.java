package com.example.ermdemo.controller;

import com.example.ermdemo.constant.ResponseStatusCode;
import com.example.ermdemo.dto.LeaveDto;
import com.example.ermdemo.error.ErrorResponse;
import com.example.ermdemo.response.LeaveResponse;
import com.example.ermdemo.response.MultiSuccessResponse;
import com.example.ermdemo.response.SingleSuccessResponse;
import com.example.ermdemo.service.LeaveService;
import com.example.ermdemo.util.UtilityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/leaves")
public class LeaveController {

    private LeaveService leaveService;

    public LeaveController(LeaveService leaveService) {
        this.leaveService = leaveService;
    }

    @PostMapping
    @ApiOperation(value = "Apply for leave", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> addLevel(@RequestBody LeaveDto leaveDto) {
        LeaveResponse leaveResponse = leaveService.addLevel(leaveDto);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Level created successfully", leaveResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Get list of leaves", response = MultiSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = MultiSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getAllLevel(){
        List<LeaveResponse> leaveResponses = leaveService.findAllLevels();
        MultiSuccessResponse successResponse = new MultiSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", leaveResponses);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a leave of an employee by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getLevel(@PathVariable Long id){
        LeaveResponse leaveResponse = leaveService.findLevel(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", leaveResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }


    @PutMapping("/{id}")
    @ApiOperation(value = "Alter an employee's leave", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> updateLevel(@RequestBody LeaveDto leaveDto, @PathVariable Long id){
        LeaveResponse leaveResponse = leaveService.updateLevel(leaveDto, id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Leave altered successfully", leaveResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

//    @DeleteMapping("/{id}")
//    @ApiOperation(value = "Delete a level by Id", response = SingleSuccessResponse.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
//            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
//    public ResponseEntity<?> deleteLevel(@PathVariable Long id) {
//        leaveService.deleteLevel(id);
//        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
//                ResponseStatusCode.OK, "Level successfully deleted", null);
//        return new ResponseEntity<>(successResponse, HttpStatus.OK);
//    }
}
