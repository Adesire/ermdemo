package com.example.ermdemo.controller;

import com.example.ermdemo.constant.ResponseStatusCode;
import com.example.ermdemo.dto.LevelDto;
import com.example.ermdemo.error.ErrorResponse;
import com.example.ermdemo.response.LevelResponse;
import com.example.ermdemo.response.MultiSuccessResponse;
import com.example.ermdemo.response.SingleSuccessResponse;
import com.example.ermdemo.service.LevelService;
import com.example.ermdemo.util.UtilityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "")
public class LevelController {

    private LevelService levelService;

    public LevelController(LevelService levelService) {
        this.levelService = levelService;
    }

    @PostMapping("/levels")
    @ApiOperation(value = "Add new level to the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> addLevel(@RequestBody LevelDto levelDto) {
        LevelResponse levelResponse = levelService.addLevel(levelDto);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Level created successfully", levelResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/levels")
    @ApiOperation(value = "Get list of levels", response = MultiSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = MultiSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getAllLevel(){
        List<LevelResponse> levelResponses = levelService.findAllLevels();
        MultiSuccessResponse successResponse = new MultiSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", levelResponses);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/levels/{id}")
    @ApiOperation(value = "Get a level by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getLevel(@PathVariable Long id){
        LevelResponse levelResponse = levelService.findLevel(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", levelResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }


    @PutMapping("/levels/{id}")
    @ApiOperation(value = "Update a level in the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> updateLevel(@RequestBody LevelDto levelDto, @PathVariable Long id){
        LevelResponse levelResponse = levelService.updateLevel(levelDto, id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Level updated successfully", levelResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @DeleteMapping("/levels/{id}")
    @ApiOperation(value = "Delete a level by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> deleteLevel(@PathVariable Long id) {
        levelService.deleteLevel(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK, "Level successfully deleted", null);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }
}
