package com.example.ermdemo.controller;

import com.example.ermdemo.constant.ResponseStatusCode;
import com.example.ermdemo.dto.RoleDto;
import com.example.ermdemo.error.ErrorResponse;
import com.example.ermdemo.response.MultiSuccessResponse;
import com.example.ermdemo.response.PermissionResponse;
import com.example.ermdemo.response.RoleResponse;
import com.example.ermdemo.response.SingleSuccessResponse;
import com.example.ermdemo.service.PermissionService;
import com.example.ermdemo.service.RoleService;
import com.example.ermdemo.util.UtilityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/roles")
public class RoleController {

    private RoleService roleService;
    private PermissionService permissionService;

    public RoleController(RoleService roleService, PermissionService permissionService) {
        this.roleService = roleService;
        this.permissionService = permissionService;
    }


    @GetMapping
    @ApiOperation(value = "Get list of roles", response = MultiSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = PermissionResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getAllRoles(){

        List<RoleResponse> roleResponses = roleService.findAll();
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"successful", roleResponses);

        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Update a role in the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> findRole(@PathVariable Long id){
        RoleResponse roleResponse = roleService.findRole(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", roleResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation(value = "Add new role to the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> addRole(@RequestBody RoleDto roleDto) {
        RoleResponse roleResponse = roleService.createRole(roleDto);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Level created successfully", roleResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update a role in the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> updateDepartment(@RequestBody RoleDto roleDto, @PathVariable Long id){
        RoleResponse departmentResponse = roleService.updateRole(roleDto, id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Department updated successfully", departmentResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/permissions")
    @ApiOperation(value = "Get list of permissions", response = MultiSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = PermissionResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getAllPermissions(){

        List<PermissionResponse> permissionResponses = permissionService.findAll();
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"successful", permissionResponses);

        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

//    @GetMapping("/{id}")
//    @ApiOperation(value = "Get a Department by Id", response = SingleSuccessResponse.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
//            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
//    public ResponseEntity<?> getDepartment(@PathVariable Long id){
//        DepartmentResponse departmentResponse = departmentService.findDepartment(id);
//        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
//                ResponseStatusCode.OK,"Successful", departmentResponse);
//        return new ResponseEntity<>( successResponse, HttpStatus.OK);
//    }


//    @PutMapping("/{id}")
//    @ApiOperation(value = "Update a department in the database", response = SingleSuccessResponse.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
//            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
//    public ResponseEntity<?> updateDepartment(@RequestBody DepartmentDto departmentDto, @PathVariable Long id){
//        DepartmentResponse departmentResponse = departmentService.updateDepartment(departmentDto, id);
//        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
//                ResponseStatusCode.OK,"Department updated successfully", departmentResponse);
//        return new ResponseEntity<>( successResponse, HttpStatus.OK);
//    }

//    @DeleteMapping("/{id}")
//    @ApiOperation(value = "Delete a department by Id", response = SingleSuccessResponse.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
//            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
//            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
//    public ResponseEntity<?> deleteDepartment(@PathVariable Long id){
//        departmentService.deleteLevel(id);
//        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
//                ResponseStatusCode.OK,"Level successfully deleted", null);
//        return new ResponseEntity<>( successResponse, HttpStatus.OK);
//    }

}
