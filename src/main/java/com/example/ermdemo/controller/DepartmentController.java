package com.example.ermdemo.controller;

import com.example.ermdemo.constant.ResponseStatusCode;
import com.example.ermdemo.dto.DepartmentDto;
import com.example.ermdemo.error.ErrorResponse;
import com.example.ermdemo.response.DepartmentResponse;
import com.example.ermdemo.response.MultiSuccessResponse;
import com.example.ermdemo.response.SingleSuccessResponse;
import com.example.ermdemo.service.DepartmentService;
import com.example.ermdemo.util.UtilityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/departments")
public class DepartmentController {

    private DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping
    @ApiOperation(value = "Add new Department to the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> addDepartment(@RequestBody DepartmentDto departmentDto) {
        DepartmentResponse departmentResponse = departmentService.addDepartment(departmentDto);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Level created successfully", departmentResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Get list of Departments", response = MultiSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = MultiSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getAllDepartment(){
        List<DepartmentResponse> departmentResponses = departmentService.findAllDepartments();
        MultiSuccessResponse successResponse = new MultiSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", departmentResponses);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a Department by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getDepartment(@PathVariable Long id){
        DepartmentResponse departmentResponse = departmentService.findDepartment(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", departmentResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }


    @PutMapping("/{id}")
    @ApiOperation(value = "Update a department in the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> updateDepartment(@RequestBody DepartmentDto departmentDto, @PathVariable Long id){
        DepartmentResponse departmentResponse = departmentService.updateDepartment(departmentDto, id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Department updated successfully", departmentResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a department by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> deleteDepartment(@PathVariable Long id){
        departmentService.deleteLevel(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Level successfully deleted", null);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }
}
