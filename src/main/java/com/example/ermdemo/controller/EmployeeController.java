package com.example.ermdemo.controller;

import com.example.ermdemo.constant.ResponseStatusCode;
import com.example.ermdemo.dto.EmployeeDto;
import com.example.ermdemo.dto.LoginDto;
import com.example.ermdemo.dto.RefreshTokenDto;
import com.example.ermdemo.error.ErrorResponse;
import com.example.ermdemo.error.TokenExpiredException;
import com.example.ermdemo.model.Employee;
import com.example.ermdemo.response.*;
import com.example.ermdemo.security.AccessTokenDetails;
import com.example.ermdemo.security.RefreshTokenDetails;
import com.example.ermdemo.service.EmployeeService;
import com.example.ermdemo.util.UtilityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(path = "/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/login")
    @ApiOperation(value = "Get an employee by Staff Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto) {
        EmployeeLoginResponse employeeLoginResponse = employeeService.findEmployeeAndLogin(loginDto);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", employeeLoginResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @PostMapping("/refresh-token")
    @ApiOperation(value = "", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "",response = EmployeeLoginResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> refreshToken(@RequestBody RefreshTokenDto refreshTokenDto){

        EmployeeLoginResponse employeeLoginResponse = employeeService.refreshToken(refreshTokenDto);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", employeeLoginResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation(value = "Add new employee to the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> addEmployee(@RequestBody EmployeeDto employeeDto){
        EmployeeResponse employeeResponse = employeeService.addEmployee(employeeDto);

        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"User created successfully", employeeResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping
//    @PreAuthorize("hasAnyAuthority('CREATE_USER','EDIT_USER','DELETE_USER')")
    @ApiOperation(value = "Get paginated list of all employees", response = MultiSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = MultiSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getAllEmployee(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "20") int size){
        List<EmployeeResponse> employeeResponses = employeeService.findEmployees(page, size);
        MultiSuccessResponse successResponse = new MultiSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", employeeResponses);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get an employee by Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getEmployee(@PathVariable Long id){
        EmployeeResponse employeeResponse = employeeService.findEmployee(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", employeeResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }


    @PutMapping("/{id}")
    @ApiOperation(value = "Update an employee's information in the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> updateEmployee(@RequestBody EmployeeDto employeeDto, @PathVariable Long id){
        EmployeeResponse employeeResponse = employeeService.updateEmployee(employeeDto, id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Employee updated successfully", employeeResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @PutMapping("/active")
    @ApiOperation(value = "return Active employees from the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> active(){
        EmployeeResponse employeeResponse = employeeService.findByActive();
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", employeeResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @PutMapping("/activate/{id}")
    @ApiOperation(value = "Activate and Deactivate an employee in the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> activateDeactivateEmployee(@PathVariable Long id, @RequestParam boolean active){
        EmployeeResponse employeeResponse = employeeService.activateDeactivateEmployee(id, active);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Employee deactivate successfully", employeeResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete an employee's information from the database", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> deleteEmployee(@PathVariable Long id){
        employeeService.deleteEmployee(id);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Employee deleted successfully", null);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/directory")
    @ApiOperation(value = "Get paginated list of all employees", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = MultiSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getEmployeesDirectory(@RequestParam(defaultValue = "true") boolean active, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "20") int size){
        List<EmployeeDirectoryResponse> employeeResponses = employeeService.findEmployeeDirectory(active, page, size);
        MultiSuccessResponse successResponse = new MultiSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", employeeResponses);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }

    @GetMapping("/directory/{staffId}")
    @ApiOperation(value = "Get an employee by Staff Id", response = SingleSuccessResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "",response = SingleSuccessResponse.class),
            @ApiResponse(code = 400, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "",response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "",response = ErrorResponse.class)})
    public ResponseEntity<?> getEmployee(@PathVariable String staffId) {
        EmployeeResponse employeeResponse = employeeService.findEmployeeByStaffId(staffId);
        SingleSuccessResponse successResponse = new SingleSuccessResponse(UtilityService.getCurrentTimeStamp(),
                ResponseStatusCode.OK,"Successful", employeeResponse);
        return new ResponseEntity<>( successResponse, HttpStatus.OK);
    }
}
