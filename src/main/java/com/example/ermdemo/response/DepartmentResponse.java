package com.example.ermdemo.response;

import lombok.Data;

@Data
public class DepartmentResponse {
    private Long id;
    private String name;
    private long head;
}
