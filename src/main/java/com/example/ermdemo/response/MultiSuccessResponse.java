package com.example.ermdemo.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MultiSuccessResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date timestamp;
    private String responseCode;
    private String message;
    private List<Object> data = new ArrayList();

    public MultiSuccessResponse(Date timestamp, String responseCode, String message, Object data) {
        this.timestamp = timestamp;
        this.responseCode = responseCode;
        this.message = message;
        List<Object> objects = new ArrayList<>();
        objects.add(data);
        this.setData(objects);
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> datas) {
        datas.stream().forEach(data -> {
            this.data.add(data);
        });

    }

}
