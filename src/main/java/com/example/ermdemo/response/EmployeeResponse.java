package com.example.ermdemo.response;

import com.example.ermdemo.model.Department;
import com.example.ermdemo.model.Level;
import com.example.ermdemo.model.LineManager;
import com.example.ermdemo.model.Role;
import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
@Data
public class EmployeeResponse {
    private long id;
    private String staffId;
    private String lastName;
    private String firstName;
    private String otherNames;
    private String mobileNo;
    private String phoneNo;
    private String email;
//    private String companyEmail;
    private int isMale;
    private LocalDateTime dateOfBirth;
    private Department department;
    private String designation;
    private Level level;
    private LineManager lineManager;
    private int isLineManager;
    private int isDepartmentHead;
    private LocalDateTime employeeStartDate;
    private String imageUpload;
    //private List<String> otherDocs;
    private Role role;
    private long lastLogin;
    private long createdBy;
    private boolean active = false;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
