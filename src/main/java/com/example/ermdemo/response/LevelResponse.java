package com.example.ermdemo.response;

import lombok.Data;

@Data
public class LevelResponse {
    private Long id;
    private String name;
    private int annualLeaveDays;
}
