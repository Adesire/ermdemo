package com.example.ermdemo.response;

import com.example.ermdemo.model.Permission;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collection;

@Data
public class RoleResponse {
    private long id;
    private String name;

    private Collection<Permission> permissions;
    private Long createdBy;
    private Long updatedBy;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
