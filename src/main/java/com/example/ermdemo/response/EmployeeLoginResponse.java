package com.example.ermdemo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeLoginResponse {
    private EmployeeResponse employee;
    private String accessToken;
    private String refreshToken;
    private long accessTokenExpireTime;
    private long refreshTokenExpireTime;
}
