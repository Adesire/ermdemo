package com.example.ermdemo.response;

import com.example.ermdemo.model.Department;
import com.example.ermdemo.model.Level;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class EmployeeDirectoryResponse {

    private String fullName;
    private String email;
    private String mobileNo;
    private int isMale;
    private Department department;
    private String designation;
    private Level level;
    private LocalDateTime dateOfBirth;
    private LocalDateTime employeeStartDate;

    private String imageUpload;
}
