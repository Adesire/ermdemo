package com.example.ermdemo.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class SingleSuccessResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date timestamp;
    private String responseCode;
    private String message;
    private Object data;

    public SingleSuccessResponse(Date timestamp, String responseCode, String message) {
        this.timestamp = timestamp;
        this.responseCode = responseCode;
        this.message = message;
    }

    public SingleSuccessResponse(Date timestamp, String responseCode, String message, Object data) {
        this.timestamp = timestamp;
        this.responseCode = responseCode;
        this.message = message;
        this.setData(data);
    }
}
