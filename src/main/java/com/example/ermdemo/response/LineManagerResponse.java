package com.example.ermdemo.response;

import com.example.ermdemo.model.Employee;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LineManagerResponse {
    private long id;
    private Employee employee;
    private Long createdBy;
    private Long updatedBy;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
