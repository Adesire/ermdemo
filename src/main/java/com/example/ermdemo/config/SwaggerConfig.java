package com.example.ermdemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.VendorExtension;
import springfox.documentation.service.Contact;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket apiDocket() {

        Contact contact = new Contact("Philip Akpeki", "www.devphilip.com", "philipakpeki@gmail.com");

        @SuppressWarnings("rawtypes")
        ApiInfo apiInfo = new ApiInfo("DevPhilip REST API Documentation",
                "This Api Documentationis a property of DevPhilip.",
                "API v1.0",
                "Terms of service",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<VendorExtension>());

        Docket docket =  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .securitySchemes(Arrays.asList(apiKey()))
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

    private ApiKey[] apiKey(){
        return new ApiKey[]{
                new ApiKey("Authorization","Authorization","header"),
                new ApiKey("Refresh-Token","refreshToken","header")
        };
    }
}