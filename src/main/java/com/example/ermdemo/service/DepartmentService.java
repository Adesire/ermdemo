package com.example.ermdemo.service;

import com.example.ermdemo.dto.DepartmentDto;
import com.example.ermdemo.error.BadRequestException;
import com.example.ermdemo.model.Department;
import com.example.ermdemo.repository.DepartmentRepository;
import com.example.ermdemo.response.DepartmentResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentService {


    private DepartmentRepository departmentRepository;
    private ModelMapper modelMapper;

    public DepartmentService(DepartmentRepository departmentRepository, ModelMapper modelMapper) {
        this.departmentRepository = departmentRepository;
        this.modelMapper = modelMapper;
    }

    public DepartmentResponse addDepartment(DepartmentDto departmentDto) {
        if (departmentDto == null) throw new BadRequestException("Department cannot empty");
        Department department = modelMapper.map(departmentDto, Department.class);
        Department savedDepartment = departmentRepository.save(department);
        return modelMapper.map(savedDepartment, DepartmentResponse.class);
    }

    public List<DepartmentResponse> findAllDepartments() {
        List<Department> departments = departmentRepository.findAll();
        List<DepartmentResponse> departmentResponses = new ArrayList<>();

        if(departments != null && !departments.isEmpty()) {
            Type listType = new TypeToken<List<DepartmentResponse>>() {}.getType();
            departmentResponses = modelMapper.map(departments, listType);
        }
        return departmentResponses;
    }

    public DepartmentResponse findDepartment(Long id) {
        Department department = departmentRepository.findById(id).orElse(null);
        if (department == null) throw new BadRequestException("Department with Id: " + id + " not found");
        return modelMapper.map(department, DepartmentResponse.class);
    }

    public DepartmentResponse findDepartmentByname(String name) {
        Department department = departmentRepository.findByName(name).orElse(null);
        if (department == null) throw new BadRequestException("Department with name: " + name + " not found");
        return modelMapper.map(department, DepartmentResponse.class);
    }

    public DepartmentResponse updateDepartment(DepartmentDto departmentDto, Long id) {
        if (departmentDto == null) throw new BadRequestException("Department cannot empty");
        Department department = departmentRepository.findById(id).orElse(null);
        if (department == null) throw new BadRequestException("Department with Id: " + id + " not found");
        department = modelMapper.map(departmentDto, Department.class);
        Department savedDepartment = departmentRepository.save(department);
        return modelMapper.map(savedDepartment, DepartmentResponse.class);
    }

    public void deleteLevel(Long id) {
        Department department = departmentRepository.findById(id).orElse(null);
        if (department == null) throw new BadRequestException("Department with Id: " + id + " not found");
        departmentRepository.delete(department);
    }
}
