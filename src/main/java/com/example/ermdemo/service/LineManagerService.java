package com.example.ermdemo.service;

import com.example.ermdemo.dto.LineManagerDto;
import com.example.ermdemo.error.BadRequestException;
import com.example.ermdemo.model.Employee;
import com.example.ermdemo.model.LineManager;
import com.example.ermdemo.repository.LineManagerRepository;
import com.example.ermdemo.response.LevelResponse;
import com.example.ermdemo.response.LineManagerResponse;
import com.example.ermdemo.util.UtilityService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
public class LineManagerService {

    @Autowired
    private LineManagerRepository lineManagerRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private UtilityService utilityService;


    public LineManagerResponse addLineManager(LineManagerDto levelDto) {
        if (levelDto == null) throw new BadRequestException("Cannot add empty Line Manager record");
        LineManager lineManager = modelMapper.map(levelDto, LineManager.class);
        Employee employee = utilityService.getLoggedInEmployee();
        lineManager.setCreatedBy(employee.getId());
        LineManager savedLineManager = lineManagerRepository.save(lineManager);
        return modelMapper.map(savedLineManager, LineManagerResponse.class);
    }

    public List<LineManagerResponse> findAllLineManagers() {
        List<LineManager> lineManagers = lineManagerRepository.findAll();
        List<LineManagerResponse> lineManagerResponses = new ArrayList<>();
        if (lineManagers != null && !lineManagers.isEmpty()) {
            Type listType = new TypeToken<List<LevelResponse>>() {}.getType();
            lineManagerResponses = new ModelMapper().map(lineManagers, listType);
        }
        return lineManagerResponses;
    }

    public LineManagerResponse findLineManager(Long id) {
        LineManager lineManagers = lineManagerRepository.findById(id).orElse(null);
        if (lineManagers == null) throw new BadRequestException("Line Manager with Id: " + id + " not found");
        return modelMapper.map(lineManagers, LineManagerResponse.class);
    }

    public LineManagerResponse updateLineManager(LineManagerDto lineManagerDto, Long id) {
        if (lineManagerDto == null) throw new BadRequestException("Line Manager Cannot be blank");
        LineManager lineManager = lineManagerRepository.findById(id).orElse(null);
        Employee employee = utilityService.getLoggedInEmployee();
        lineManager.setUpdatedBy(employee.getId());
        if (lineManager == null) throw new BadRequestException("Line Manager with Id: " + id + " not found");
        LineManager savedLineManager = lineManagerRepository.save(modelMapper.map(lineManagerDto, LineManager.class));
        return modelMapper.map(savedLineManager, LineManagerResponse.class);
    }

    public void deleteLineManager(Long id) {
        LineManager lineManager = lineManagerRepository.findById(id).orElse(null);
        if (lineManager == null) throw new BadRequestException("Line Manager with Id: " + id + " not found");
        lineManagerRepository.delete(lineManager);
    }


}
