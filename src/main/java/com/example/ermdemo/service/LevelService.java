package com.example.ermdemo.service;

import com.example.ermdemo.dto.LevelDto;
import com.example.ermdemo.error.BadRequestException;
import com.example.ermdemo.model.Level;
import com.example.ermdemo.repository.LevelRepository;
import com.example.ermdemo.response.LevelResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
public class LevelService {

    private LevelRepository levelRepository;
    private ModelMapper modelMapper;

    public LevelService(LevelRepository levelRepository, ModelMapper modelMapper) {
        this.levelRepository = levelRepository;
        this.modelMapper = modelMapper;
    }

    public LevelResponse addLevel(LevelDto levelDto) {
        if (levelDto == null) throw new BadRequestException("Cannot add empty level record");
        Level level = modelMapper.map(levelDto, Level.class);
        Level savedLevel = levelRepository.save(level);
        return modelMapper.map(savedLevel, LevelResponse.class);
    }

    public List<LevelResponse> findAllLevels() {
        List<Level> levels = levelRepository.findAll();
        List<LevelResponse> levelResponses = new ArrayList<>();
        if (levels != null && !levels.isEmpty()) {
            Type listType = new TypeToken<List<LevelResponse>>() {}.getType();
            levelResponses = new ModelMapper().map(levels, listType);
        }
        return levelResponses;
    }

    public LevelResponse findLevel(Long id) {
        Level level = levelRepository.findById(id).orElse(null);
        if (level == null) throw new BadRequestException("Level with Id: " + id + " not found");
        return modelMapper.map(level, LevelResponse.class);
    }

    public LevelResponse findByName(String name) {
        Level level = levelRepository.findByName(name).orElse(null);
        if (level == null) throw new BadRequestException("Level with name: " + name + " not found");
        return modelMapper.map(level, LevelResponse.class);
    }

    public LevelResponse updateLevel(LevelDto levelDto, Long id) {
        if (levelDto == null) throw new BadRequestException("Level Cannot be blank");
        Level level = levelRepository.findById(id).orElse(null);
        if (level == null) throw new BadRequestException("Level with Id: " + id + " not found");
        Level savedLevel = levelRepository.save(modelMapper.map(levelDto, Level.class));
        return modelMapper.map(savedLevel, LevelResponse.class);
    }

    public void deleteLevel(Long id) {
        Level level = levelRepository.findById(id).orElse(null);
        if (level == null) throw new BadRequestException("Level with Id: " + id + " not found");
        levelRepository.delete(level);
    }
}
