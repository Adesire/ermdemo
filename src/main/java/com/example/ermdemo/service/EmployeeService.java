package com.example.ermdemo.service;

import com.example.ermdemo.dto.EmployeeDto;
import com.example.ermdemo.dto.LoginDto;
import com.example.ermdemo.dto.RefreshTokenDto;
import com.example.ermdemo.error.BadRequestException;
import com.example.ermdemo.error.TokenExpiredException;
import com.example.ermdemo.model.*;
import com.example.ermdemo.repository.*;
import com.example.ermdemo.response.EmployeeDirectoryResponse;
import com.example.ermdemo.response.EmployeeLoginResponse;
import com.example.ermdemo.response.EmployeeResponse;
import com.example.ermdemo.security.AccessTokenDetails;
import com.example.ermdemo.security.EmployeeDetails;
import com.example.ermdemo.security.JwtTokenProvider;
import com.example.ermdemo.security.RefreshTokenDetails;
import com.example.ermdemo.util.UtilityService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EmployeeService {

    @Autowired
    private UtilityService utilityService;

    private EmployeeRepository employeeRepository;
    private ModelMapper modelMapper;
    private RestTemplate restTemplate;
    private JwtTokenProvider jwtTokenProvider;
    private RoleRepository roleRepository;
    private LineManagerRepository lineManagerRepository;
    private DepartmentRepository departmentRepository;
    private LevelRepository levelRepository;

    public EmployeeService(EmployeeRepository employeeRepository, ModelMapper modelMapper, RestTemplate restTemplate,
                           JwtTokenProvider jwtTokenProvider, RoleRepository roleRepository, LineManagerRepository lineManagerRepository,
                           DepartmentRepository departmentRepository, LevelRepository levelRepository) {
        this.employeeRepository = employeeRepository;
        this.modelMapper = modelMapper;
        this.restTemplate = restTemplate;
        this.jwtTokenProvider = jwtTokenProvider;
        this.roleRepository = roleRepository;
        this.lineManagerRepository = lineManagerRepository;
        this.departmentRepository = departmentRepository;
        this.levelRepository = levelRepository;
    }

    public EmployeeResponse addEmployee(EmployeeDto employeeDto) {

        if (employeeDto == null) {
            throw new BadRequestException("You cannot save empty data to the database");
        }
        Employee employee = modelMapper.map(employeeDto, Employee.class);
        Role role = roleRepository.findById(employeeDto.getRoleId()).orElse(null);
        if (role == null) {
            throw new BadRequestException("Role should be specified");
        }
        employee.setRole(role);
        LineManager lineManager = lineManagerRepository.findById(employeeDto.getLineManagerId()).orElse(null);
//        if (lineManager == null) {
//            throw new BadRequestException("Line Manager should be specified");
//        }
        employee.setLineManager(lineManager);
        Department department = departmentRepository.findById(employeeDto.getDepartmentId()).orElse(null);
        if (department == null)
            throw new BadRequestException("Department should be specified");
        employee.setDepartment(department);

        Level level = levelRepository.findById(employeeDto.getLevelId()).orElse(null);
        if (level == null)
            throw new BadRequestException("Level cannot be Empty");
        employee.setLevel(level);

        Employee loggedInEmployee = utilityService.getLoggedInEmployee();
        employee.setCreatedBy(loggedInEmployee.getId());

        Employee savedEmployee = employeeRepository.save(employee);

        return modelMapper.map(savedEmployee, EmployeeResponse.class);
    }

    public List<EmployeeResponse> findEmployees(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Employee> employees = employeeRepository.findAll(pageable).getContent();
        List<EmployeeResponse> employeeResponses = new ArrayList<>();

        if (employees != null && !employees.isEmpty()) {
            Type listType = new TypeToken<List<EmployeeResponse>>() {
            }.getType();
            employeeResponses = new ModelMapper().map(employees, listType);
        }

        return employeeResponses;
    }

    public EmployeeResponse findEmployee(Long id) {
        if (id == null) {
            throw new BadRequestException("User not found");
        }
        Employee employee = employeeRepository.findById(id).get();
        return modelMapper.map(employee, EmployeeResponse.class);
    }

    public EmployeeResponse updateEmployee(EmployeeDto employeeDto, Long id) {

        Employee employee = employeeRepository.findById(id).orElse(null);
        if (employee == null) {
            throw new BadRequestException("Employee not found");
        }
        if (employeeDto == null) {
            throw new BadRequestException("You cannot save empty data to the database");
        }

        Employee savedEmployee = employeeRepository.save(modelMapper.map(employeeDto, Employee.class));
        return modelMapper.map(savedEmployee, EmployeeResponse.class);
    }

    public boolean checkIfEmployeeExists(String email) {
        return employeeRepository.countEmployeeByEmail(email) > 0;
    }

    public Employee findByEmail(String email) {
        return employeeRepository.findByEmail(email).orElse(null);
    }

    public void deleteEmployee(Long id) {
        Employee employee = employeeRepository.findById(id).orElse(null);
        employeeRepository.delete(employee);
    }

    public boolean isUserEmpty() {
        return IterableUtils.isEmpty(employeeRepository.findAll());
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    public EmployeeResponse activateDeactivateEmployee(Long id, boolean active) {
        Employee employee = employeeRepository.findById(id).orElse(null);
        if (employee == null) {
            throw new BadRequestException("Employee not found");
        }
        employee.setActive(active);
        employee = employeeRepository.save(employee);
        return modelMapper.map(employee, EmployeeResponse.class);
    }

    public List<EmployeeDirectoryResponse> findEmployeeDirectory(boolean active, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<EmployeeDirectoryResponse> directoryResponses = new ArrayList<>();
        List<Employee> employees = employeeRepository.findAllByIsActive(active, pageable).getContent();

        if (employees != null && !employees.isEmpty()) {
            Type listType = new TypeToken<List<EmployeeResponse>>() {
            }.getType();
            directoryResponses = new ModelMapper().map(employees, listType);
        }

        return directoryResponses;
    }

    public EmployeeResponse findEmployeeByStaffId(String staffId) {
        Employee employee = employeeRepository.findByStaffId(staffId).orElse(null);
        if (employee == null) {
            throw new BadRequestException("Employee not found");
        }
        return modelMapper.map(employee, EmployeeResponse.class);
    }

    public EmployeeResponse findByActive() {
        return null;
    }

    public EmployeeLoginResponse findEmployeeAndLogin(LoginDto loginDto) {

        String tokenUrl = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=";
        try {
            tokenUrl += loginDto.getGoogleIdToken();
        } catch (BadRequestException e) {
            throw new BadRequestException("Invalid user Id token");
        }

        Map<String, Object> map = restTemplate.getForObject(tokenUrl, Map.class);

        String email = String.valueOf(map.get("email"));
//        boolean email_verified = Boolean.valueOf(map.get("email_verified").toString());

        Employee employee = employeeRepository.findByEmail(email).orElse(null);
        if (employee == null) {
            throw new BadRequestException("Your are not allowed to use this system");
        }

        employee.setLastLogin(UtilityService.getCurrentTimeStamp());
        employee = employeeRepository.save(employee);

        AccessTokenDetails accessTokenDetails = this.jwtTokenProvider.generateToken(new EmployeeDetails(employee));
        RefreshTokenDetails refreshTokenDetails = this.jwtTokenProvider.generateRefreshToken(new EmployeeDetails(employee));

        EmployeeResponse employeeResponse = modelMapper.map(employee, EmployeeResponse.class);
        return new EmployeeLoginResponse(employeeResponse, accessTokenDetails.getAccessToken(),
                refreshTokenDetails.getRefreshToken(), accessTokenDetails.getExpireTime(), refreshTokenDetails.getExpireTime());
    }

    public EmployeeLoginResponse refreshToken(RefreshTokenDto refreshTokenDto) {
        boolean isValid = jwtTokenProvider.validateToken(refreshTokenDto.getRefreshToken());

        if (!isValid) {
            throw new TokenExpiredException("Token expired");
        }

        long employeeId = this.jwtTokenProvider.getUserIdFromJWT(refreshTokenDto.getRefreshToken());
        Employee employee = this.employeeRepository.findById(employeeId).orElse(null);

        AccessTokenDetails accessTokenDetails = this.jwtTokenProvider.generateToken(new EmployeeDetails(employee));
        RefreshTokenDetails refreshTokenDetails = this.jwtTokenProvider.generateRefreshToken(new EmployeeDetails(employee));

        EmployeeResponse employeeResponse = modelMapper.map(employee, EmployeeResponse.class);
        return new EmployeeLoginResponse(employeeResponse, accessTokenDetails.getAccessToken(),
                refreshTokenDetails.getRefreshToken(), accessTokenDetails.getExpireTime(), refreshTokenDetails.getExpireTime());

    }
}
