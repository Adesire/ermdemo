package com.example.ermdemo.service;

import com.example.ermdemo.dto.RoleDto;
import com.example.ermdemo.error.BadRequestException;
import com.example.ermdemo.model.Employee;
import com.example.ermdemo.model.Permission;
import com.example.ermdemo.model.Role;
import com.example.ermdemo.repository.PermissionRepository;
import com.example.ermdemo.repository.RoleRepository;
import com.example.ermdemo.response.RoleResponse;
import com.example.ermdemo.util.UtilityService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private UtilityService utilityService;
    @Autowired
    private ModelMapper modelMapper;

    public Role save(Role role) {
        return this.roleRepository.save(role);
    }

    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }

    public List<RoleResponse> findAll() {
        List<Role> roles = roleRepository.findAll();
        List<RoleResponse> roleResponses = new ArrayList<>();

        if (roles != null && !roleResponses.isEmpty()) {
            Type listType = new TypeToken<List<RoleResponse>>() {
            }.getType();
            roleResponses = new ModelMapper().map(roles, listType);
        }

        return roleResponses;
    }

    public RoleResponse findRole(Long id) {
        Role role = this.roleRepository.findById(id).orElse(null);
        if(role == null){
            throw new BadRequestException("Role with id: " + id + " does not exist");
        }
        return modelMapper.map(role, RoleResponse.class);
    }

    public RoleResponse createRole(RoleDto roleDto) {

        if(!StringUtils.hasText(roleDto.getName())) throw new BadRequestException("Role name is required");
        if(roleDto.getPermissions() == null || roleDto.getPermissions().size() == 0) {
            throw new BadRequestException("At least one permission is required to create a role");
        }
        Role role = this.roleRepository.findByName(roleDto.getName());
        if(role != null){
            throw new BadRequestException("Role name already exist");
        }

        Employee employee = utilityService.getLoggedInEmployee();

        List<Permission> permissions = roleDto.getPermissions().stream().map(pId -> {
            Permission permission = permissionRepository.findById(pId).orElse(null);
            if(permission == null){
                throw new BadRequestException("Permission with id: " + pId + " does'nt exist");
            }
            return permission;
        }).collect(Collectors.toList());


        role = new Role();
        role.setName(roleDto.getName());
        role.setPermissions(permissions);
        role.setCreatedBy(employee.getId());

        return modelMapper.map(role, RoleResponse.class);
    }

    public RoleResponse updateRole(RoleDto roleDto, Long id) {

        Role role = this.roleRepository.findById(id).orElse(null);
        if(role == null){
            throw new BadRequestException("Role does not exist");
        }

        if(!StringUtils.hasText(roleDto.getName())) throw new BadRequestException("Role name is required");
        if(roleDto.getPermissions() == null || roleDto.getPermissions().size() == 0) {
            throw new BadRequestException("At least one permission is required to create a role");
        }

        Employee employee = utilityService.getLoggedInEmployee();

        List<Permission> permissions = roleDto.getPermissions().stream().map(pId -> {
            Permission permission = permissionRepository.findById(pId).orElse(null);
            if(permission == null){
                throw new BadRequestException("Permission with id: " + pId + " does'nt exist");
            }
            return permission;
        }).collect(Collectors.toList());


        if(!role.getName().equalsIgnoreCase(roleDto.getName())){
            if(this.roleRepository.findByName(roleDto.getName()) != null){
                throw new BadRequestException("Role name already exist");
            }
        }

        role = new Role();
        role.setName(roleDto.getName());
        role.setPermissions(permissions);
        role.setUpdatedBy(employee.getId());

        return modelMapper.map(role, RoleResponse.class);
    }
}
