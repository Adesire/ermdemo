package com.example.ermdemo.service;

import com.example.ermdemo.dto.PermissionDto;
import com.example.ermdemo.model.Employee;
import com.example.ermdemo.model.Permission;
import com.example.ermdemo.repository.PermissionRepository;
import com.example.ermdemo.response.EmployeeResponse;
import com.example.ermdemo.response.PermissionResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
public class PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<PermissionResponse> findAll() {

        List<Permission> permissions = permissionRepository.findAll();
        List<PermissionResponse> permissionResponses = new ArrayList<>();

        if (permissions != null && !permissions.isEmpty()) {
            Type listType = new TypeToken<List<PermissionResponse>>() {
            }.getType();
            permissionResponses = new ModelMapper().map(permissions, listType);
        }

        return permissionResponses;

    }

    public PermissionResponse createPermission(PermissionDto permissionDto) {
        Permission permission = modelMapper.map(permissionDto, Permission.class);
        return modelMapper.map(permission, PermissionResponse.class);
    }
}
