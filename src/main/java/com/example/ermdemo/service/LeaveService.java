package com.example.ermdemo.service;

import com.example.ermdemo.dto.LeaveDto;
import com.example.ermdemo.response.LeaveResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeaveService {


    public LeaveResponse addLevel(LeaveDto leaveDto) {
        return null;
    }

    public List<LeaveResponse> findAllLevels() {
        return null;
    }

    public LeaveResponse findLevel(Long id) {
        return null;
    }

    public LeaveResponse updateLevel(LeaveDto leaveDto, Long id) {
        return null;
    }

    public void deleteLevel(Long id) {
    }
}
