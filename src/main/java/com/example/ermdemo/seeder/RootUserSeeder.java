package com.example.ermdemo.seeder;

import com.example.ermdemo.dto.DepartmentDto;
import com.example.ermdemo.dto.LevelDto;
import com.example.ermdemo.model.*;
import com.example.ermdemo.repository.DepartmentRepository;
import com.example.ermdemo.repository.EmployeeRepository;
import com.example.ermdemo.repository.LevelRepository;
import com.example.ermdemo.repository.PermissionRepository;
import com.example.ermdemo.service.*;
import com.example.ermdemo.util.UtilityService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class RootUserSeeder implements CommandLineRunner, Ordered {

    @Value("${root.employee.email}")
    private String rootEmail;

    @Value("${root.employee.mobile}")
    private String rootMobileNo;

    @Value("${root.employee.firstname}")
    private String rootFirstName;

    @Value("${root.employee.lastname}")
    private String rootLastName;

    private RoleService roleService;
    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;
    private DepartmentService departmentService;
    private DepartmentRepository departmentRepository;
    private LevelService levelService;
    private LevelRepository levelRepository;
    private PermissionRepository permissionRepository;

    public RootUserSeeder(RoleService roleService, EmployeeService employeeService, EmployeeRepository employeeRepository,
                          DepartmentService departmentService, DepartmentRepository departmentRepository,
                          LevelService levelService, LevelRepository levelRepository, PermissionRepository permissionRepository) {
        this.roleService = roleService;
        this.employeeService = employeeService;
        this.employeeRepository = employeeRepository;
        this.departmentService = departmentService;
        this.departmentRepository = departmentRepository;
        this.levelService = levelService;
        this.levelRepository = levelRepository;
        this.permissionRepository = permissionRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        createDepartment();
        createLevel();
        createPermissions();
        createRootUser();
    }

    private void createDepartment() {
        List<DepartmentDto> departments = new ArrayList<>();
        DepartmentDto department1 = new DepartmentDto();
        department1.setName("Administration");
        departments.add(department1);

        DepartmentDto department2 = new DepartmentDto();
        department2.setName("Business Development");
        departments.add(department2);

        DepartmentDto department3 = new DepartmentDto();
        department3.setName("Development");
        departments.add(department3);

        departments.forEach(department -> {
            Department returnedDepartment = departmentRepository.findByName(department.getName()).orElse(null);
            if (returnedDepartment == null) departmentService.addDepartment(department);
        });
    }

    private void createLevel() {
        List<LevelDto> levels = new ArrayList<>();
        LevelDto level1 = new LevelDto();
        level1.setName("CONSULTANT");
        level1.setAnnualLeaveDays(22);
        levels.add(level1);

        LevelDto level2 = new LevelDto();
        level2.setName("SENIOR CONSULTANT");
        level2.setAnnualLeaveDays(26);
        levels.add(level2);

        LevelDto level3 = new LevelDto();
        level3.setName("ANALYST 2");
        level3.setAnnualLeaveDays(25);
        levels.add(level3);

        levels.forEach(level -> {
            Level returnedLevel = levelRepository.findByName(level.getName()).orElse(null);
            if (returnedLevel == null) levelService.addLevel(level);
        });
    }

    private void createPermissions() {

        List<String> permissions = new ArrayList<>();

        permissions.add("CREATE_EMPLOYEE");
        permissions.add("EDIT_EMPLOYEE");
        permissions.add("DELETE_EMPLOYEE");
        permissions.add("BLOCK_AND_UNBLOCK_EMPLOYEE");
        permissions.add("CREATE_ROLE");
        permissions.add("EDIT_ROLE");
        permissions.add("DELETE_ROLE");
        permissions.add("VIEW_REPORT");
        permissions.add("DOWNLOAD_REPORT");

        for (String permissionString : permissions) {
            permissionRepository.save(new Permission(permissionString));
        }
    }

    private void createRootUser() {

        boolean isEmpty = this.employeeService.isUserEmpty();

        if (isEmpty) {
            Employee rootEmployee = new Employee();
            rootEmployee.setLastName(rootLastName);
            rootEmployee.setFirstName(rootFirstName);
            rootEmployee.setMobileNo(UtilityService.formatMobileToE164(rootMobileNo));
            rootEmployee.setEmail(rootEmail);
            rootEmployee.setIsMale(1);
            rootEmployee.setActive(true);
            Employee savedEmployee = employeeRepository.save(rootEmployee);
            Role role = this.createRootRole(savedEmployee);
            savedEmployee.setRole(role);
            this.employeeRepository.save(savedEmployee);
        }
    }

    private Role createRootRole(Employee employee) {

        List<Permission> permissions = this.permissionRepository.findAll();

        Role rootRole = this.roleService.findByName("Super Admin");

        if (rootRole == null) {
            rootRole = new Role();
            rootRole.setName("Super Admin");
//            rootRole.setCreatedBy(employee);
            rootRole.setPermissions(permissions);
            rootRole = this.roleService.save(rootRole);
        }
        return rootRole;
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
