package com.example.ermdemo.constant;

public class ResponseStatusCode {

    public static final String OK = "00";
    public static final String TOKEN_EXPIRED = "E4001";
    public static final String BAD_REQUEST = "E4000";
    public static final String UNAUTHORIZED = "E4003";
    public static final String NOT_FOUND = "E4001";
    public static final String SERVER_ERROR = "E5000";

}
