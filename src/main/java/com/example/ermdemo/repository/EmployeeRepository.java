package com.example.ermdemo.repository;

import com.example.ermdemo.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {

    int countEmployeeByEmail(String email);

    Optional<Employee> findByEmail(String email);

    Optional<Employee> findByStaffId(String staffId);

    @Query(value = "SELECT e from Employee e where e.active = ?1")
    Page<Employee> findAllByIsActive(boolean active, Pageable pageable);

}
