package com.example.ermdemo.repository;

import com.example.ermdemo.model.LineManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LineManagerRepository extends JpaRepository<LineManager, Long> {

}
