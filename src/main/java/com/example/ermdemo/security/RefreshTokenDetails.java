package com.example.ermdemo.security;

public class RefreshTokenDetails {

    private String refreshToken;
    private long expireTime;

    public RefreshTokenDetails(String refreshToken, long expireTime) {
        this.refreshToken = refreshToken;
        this.expireTime = expireTime;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }
}
