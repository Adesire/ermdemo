package com.example.ermdemo.security;

import com.example.ermdemo.error.TokenExpiredException;
import com.example.ermdemo.util.UtilityService;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private long jwtExpirationInMs;
    @Value("${app.jwtRefreshTokenInMs}")
    private long jwtRefreshExpirationInMs;

    public AccessTokenDetails generateToken(EmployeeDetails employeeDetails) {

        Date expiryDate = new Date(UtilityService.getCurrentTimeStamp().getTime() + jwtExpirationInMs);

        String accessToken =  Jwts.builder()
                .setSubject(String.valueOf(employeeDetails.getEmployeeId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();

        return new AccessTokenDetails(accessToken, expiryDate.getTime());
    }

    public RefreshTokenDetails generateRefreshToken(EmployeeDetails employeeDetails) {

        Date expiryDate = new Date(UtilityService.getCurrentTimeStamp().getTime() + jwtRefreshExpirationInMs);

        String refreshToken =  Jwts.builder()
                .setSubject(String.valueOf(employeeDetails.getEmployeeId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();

        return new RefreshTokenDetails(refreshToken, expiryDate.getTime());
    }

    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
            throw new TokenExpiredException("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
            throw new TokenExpiredException("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
            throw new TokenExpiredException("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
            throw new TokenExpiredException("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
            throw new TokenExpiredException("JWT claims string is empty.");
        }

    }
}

//import com.example.ermdemo.error.TokenExpiredException;
//import com.example.ermdemo.util.UtilityService;
//import io.jsonwebtoken.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//
//@Component
//public class JwtTokenProvider {
//
//    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);
//
//    @Value("${app.jwtSecret}")
//    private String jwtSecret;
//
//    @Value("${app.jwtExpirationInMs}")
//    private long jwtExpirationInMs;
//    @Value("${app.jwtRefreshTokenInMs}")
//    private long jwtRefreshExpirationInMs;
//
//    public AccessTokenDetails generateToken(EmployeeDetails employeeDetails) {
//
//        Date expiryDate = new Date(UtilityService.getCurrentTimeStamp().getTime() + jwtExpirationInMs);
//
//        String accessToken = Jwts.builder()
//                .setSubject(String.valueOf(employeeDetails.getEmployeeId()))
//                .setIssuedAt(new Date())
//                .setExpiration(expiryDate)
//                .signWith(SignatureAlgorithm.HS512, jwtSecret)
//                .compact();
//
//        return new AccessTokenDetails(accessToken, expiryDate.getTime());
//    }
//
//    public RefreshTokenDetails generateRefreshToken(EmployeeDetails employeeDetails) {
//
//        Date expiryDate = new Date(UtilityService.getCurrentTimeStamp().getTime() + jwtRefreshExpirationInMs);
//
//        String refreshToken =  Jwts.builder()
//                .setSubject(String.valueOf(employeeDetails.getEmployeeId()))
//                .setIssuedAt(new Date())
//                .setExpiration(expiryDate)
//                .signWith(SignatureAlgorithm.HS512, jwtSecret)
//                .compact();
//
//        return new RefreshTokenDetails(refreshToken, expiryDate.getTime());
//    }
//
//    public Long getUserIdFromJWT(String token) {
//        Claims claims = Jwts.parser()
//                .setSigningKey(jwtSecret)
//                .parseClaimsJws(token)
//                .getBody();
//
//        return Long.parseLong(claims.getSubject());
//    }
//
//    public boolean validateToken(String authToken) {
//        try {
//            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
//            return true;
//        } catch (SignatureException ex) {
//            logger.error("Invalid JWT signature");
//            throw new TokenExpiredException("Invalid JWT signature");
//        } catch (MalformedJwtException ex) {
//            logger.error("Invalid JWT token");
//            throw new TokenExpiredException("Invalid JWT token");
//        } catch (ExpiredJwtException ex) {
//            logger.error("Expired JWT token");
//            throw new TokenExpiredException("Expired JWT token");
//        } catch (UnsupportedJwtException ex) {
//            logger.error("Unsupported JWT token");
//            throw new TokenExpiredException("Unsupported JWT token");
//        } catch (IllegalArgumentException ex) {
//            logger.error("JWT claims string is empty.");
//            throw new TokenExpiredException("JWT claims string is empty.");
//        }
//
//    }
//}