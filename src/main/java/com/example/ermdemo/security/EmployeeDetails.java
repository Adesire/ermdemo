package com.example.ermdemo.security;

import com.example.ermdemo.model.Employee;
import com.example.ermdemo.model.Permission;
import com.example.ermdemo.model.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class EmployeeDetails implements UserDetails {

    private Employee employee;
    Long employeeId;
    String username;
    String firstName;
    String lastName;
//    String password;
    boolean isActive;
//    private Collection<? extends GrantedAuthority> authorities;


    public EmployeeDetails(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
        this.username = employee.getEmail();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
//        this.password = employee.getPassword();
        this.isActive = employee.isActive();
//        Collection<GrantedAuthority> authorities = getGrantedAuthorities(getPrivileges(employee.getRole()));
//        this.authorities = authorities;
    }

//    private List<String> getPrivileges(Role role) {
//
//        List<String> privileges = new ArrayList<>();
//        List<Permission> collection = new ArrayList<>();
//        collection.addAll(role.getPermissions());
////        for (Role role : roles) {
////            collection.addAll(role.getPermissions());
////        }
//        for (Permission item : collection) {
//            privileges.add(item.getName());
//        }
//        return privileges;
//    }
//
//    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
//        List<GrantedAuthority> authorities = new ArrayList<>();
//        for (String privilege : privileges) {
//            authorities.add(new SimpleGrantedAuthority(privilege));
//        }
//        return authorities;
//    }
//
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return authorities;
//    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = employee.getRole() == null ? null : employee.getRole().getPermissions()
                .stream().map(permission -> new SimpleGrantedAuthority(permission.getName()))
                .collect(Collectors.toList());

        return grantedAuthorities;
    }

    public Long getEmployeeId() {
        return this.employeeId;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isActive;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isActive;
    }

}
