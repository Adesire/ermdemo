package com.example.ermdemo.security;

import com.example.ermdemo.model.Employee;
import com.example.ermdemo.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EmployeeDetailsService implements UserDetailsService {

    @Autowired
    EmployeeService employeeService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Employee employee = this.employeeService.findByEmail(s);

        if(employee == null){
            throw new UsernameNotFoundException("User cannot be found");
        }

        return new EmployeeDetails(employee);
    }

    public UserDetails loadUserById(long id) throws UsernameNotFoundException {
        Employee user = this.employeeService.findById(id);

        if(user == null){
            throw new UsernameNotFoundException("User cannot be found");
        }

        return new EmployeeDetails(user);
    }
}
