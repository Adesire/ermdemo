package com.example.ermdemo.util;

import com.example.ermdemo.model.Employee;
import com.example.ermdemo.service.EmployeeService;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.*;
import java.util.*;

@Component
public class UtilityService {

    @Autowired
    private EmployeeService userService;

    public static Timestamp getCurrentTimeStamp(){
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Africa/Lagos"));
        Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
        return timestamp;
    }

    public Employee getLoggedInEmployee(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication != null){
            String email =  authentication.getPrincipal().toString();

            Employee employee = this.userService.findByEmail(email);

            return employee;
        }

        return null;
    }

    public static String generateOtp(int length) {
        Random random = new Random();
        String pattern = "0123456789";

        StringBuilder sb = new StringBuilder();

        return random.ints(0, pattern.length()).mapToObj(i -> pattern.charAt(i)).limit(length)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
    }

    public static String generateRandomChars(int length) {
        Random random = new Random();
        String pattern = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";

        StringBuilder sb = new StringBuilder();

        return random.ints(0, pattern.length()).mapToObj(i -> pattern.charAt(i)).limit(length)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
    }

    public static Timestamp getNext24Hours(){

        LocalDateTime today = LocalDateTime.now();

        today = today.plusHours(24);

        return Timestamp.valueOf(today);
    }

    public static Timestamp getWeekStart(){

        LocalDate today = LocalDate.now();

        LocalDate weekStart = null;
        while(today.getDayOfWeek() != DayOfWeek.MONDAY){
            today = today.minusDays(1);
        }
        weekStart = today;

        return Timestamp.valueOf(weekStart.atStartOfDay());
    }

    public static Timestamp getWeekEnd(){

        LocalDate today = LocalDate.now();

        LocalDate weekEnd = null;
        while(today.getDayOfWeek() != DayOfWeek.SUNDAY){
            today = today.plusDays(1);
        }
        weekEnd = today;

        return Timestamp.valueOf(weekEnd.atTime(23,59,59));
    }

    public static Timestamp getLastWeekStart(){

        LocalDate today = LocalDate.now().minusWeeks(1);

        LocalDate weekStart = null;
        while(today.getDayOfWeek() != DayOfWeek.MONDAY){
            today = today.minusDays(1);
        }
        weekStart = today;

        return Timestamp.valueOf(weekStart.atStartOfDay());
    }

    public static Timestamp getLastWeekEnd(){

        LocalDate today = LocalDate.now().minusWeeks(1);

        LocalDate weekEnd = null;
        while(today.getDayOfWeek() != DayOfWeek.SUNDAY){
            today = today.plusDays(1);
        }

        weekEnd = today;

        return Timestamp.valueOf(weekEnd.atTime(23,59,59));
    }

    public static Timestamp getTodayStart(){

        LocalDate today = LocalDate.now();

        return Timestamp.valueOf(today.atStartOfDay());
    }

    public static Timestamp getTodayEnd(){

        LocalDate today = LocalDate.now();

        return Timestamp.valueOf(today.atTime(23,59,59));
    }

    public static Timestamp getMonthStart(){

        LocalDate today = LocalDate.now();

        today = today.withDayOfMonth(1);

        return Timestamp.valueOf(today.atStartOfDay());
    }

    public static Timestamp getMonthEnd(){

        LocalDate today = LocalDate.now();

        int lengthOfMonth = today.lengthOfMonth();
        today = today.withDayOfMonth(lengthOfMonth);

        return Timestamp.valueOf(today.atTime(23,59,59));
    }

    public static Timestamp getNextMonthStart(){

        LocalDate today = LocalDate.now().plusMonths(1);

        today = today.withDayOfMonth(1);

        return Timestamp.valueOf(today.atStartOfDay());
    }

    public static Timestamp getNextMonthEnd(){

        LocalDate today = LocalDate.now().plusMonths(1);

        int lengthOfMonth = today.lengthOfMonth();
        today = today.withDayOfMonth(lengthOfMonth);

        return Timestamp.valueOf(today.atTime(23,59,59));
    }

    public static Timestamp getStartDateTimestamp(long startTimestamp){

        LocalDate startDate = Instant.ofEpochMilli(startTimestamp)
                .atZone(ZoneId.of("Africa/Lagos")).toLocalDate();

        startDate.atTime(0,0,1);

        return Timestamp.valueOf(startDate.atStartOfDay());
    }

    public static Timestamp getEndDateTimestamp(long endTimestamp){

        LocalDate endDate = Instant.ofEpochMilli(endTimestamp)
                .atZone(ZoneId.of("Africa/Lagos")).toLocalDate();

        LocalDateTime endDateTime = endDate.atTime(23, 59,59);
        return Timestamp.valueOf(endDateTime);
    }

    public static Timestamp addMonthToDate(int months){

        LocalDateTime now = LocalDateTime.now(ZoneId.of("Africa/Lagos")).plusMonths(months);

        return Timestamp.valueOf(now);
    }

    public static Timestamp addMonthToDate(Timestamp timestamp, int months){

        LocalDateTime now = timestamp.toLocalDateTime().plusMonths(months);

        return Timestamp.valueOf(now);
    }

    public static Timestamp getLastMonthStart(){

        LocalDate today = LocalDate.now().minusMonths(1);

        today = today.withDayOfMonth(1);

        return Timestamp.valueOf(today.atStartOfDay());
    }

    public static Timestamp getLastMonthEnd(){

        LocalDate today = LocalDate.now().minusMonths(1);

        int lengthOfMonth = today.lengthOfMonth();
        today = today.withDayOfMonth(lengthOfMonth);

        return Timestamp.valueOf(today.atTime(23,59,59));
    }

    public static Timestamp getDateStart(Timestamp timestamp){

        LocalDate localDate = timestamp.toLocalDateTime().toLocalDate();

        return Timestamp.valueOf(localDate.atStartOfDay());
    }

    public static Timestamp getTimestamp(long startTimestamp){

        LocalDateTime startDate = Instant.ofEpochMilli(startTimestamp)
                .atZone(ZoneId.of("Africa/Lagos")).toLocalDateTime();

        return Timestamp.valueOf(startDate);
    }

    public static String formatMobileToE164(String mobile){

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        String formattedMobile = null;
        try {
            Phonenumber.PhoneNumber nigerianNumberProto = phoneUtil.parse(mobile, "NG");
            formattedMobile = phoneUtil.format(nigerianNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (NumberParseException e) {
//            System.err.println("NumberParseException was thrown: " + e.toString());
        }finally {
            return formattedMobile;
        }
    }

    public static boolean isValidMobileNo(String mobile){

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        boolean isvalidNumber = false;
        try {
            Phonenumber.PhoneNumber nigerianNumberProto = phoneUtil.parse(mobile, "NG");
            isvalidNumber = phoneUtil.isValidNumberForRegion(nigerianNumberProto,"NG");
        } catch (NumberParseException e) {
//            System.err.println("NumberParseException was thrown: " + e.toString());
        }finally {
            return isvalidNumber;
        }
    }

    public static String convertToTitleCaseIteratingChars(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        StringBuilder converted = new StringBuilder();

        boolean convertNext = true;
        for (char ch : text.toCharArray()) {
            if (Character.isSpaceChar(ch)) {
                convertNext = true;
            } else if (convertNext) {
                ch = Character.toTitleCase(ch);
                convertNext = false;
            } else {
                ch = Character.toLowerCase(ch);
            }
            converted.append(ch);
        }

        return converted.toString();
    }

    public double getAudioFileLength(String s) throws IOException, UnsupportedAudioFileException, InvalidDataException, UnsupportedTagException {

        String directory = System.getProperty("java.io.tmpdir");
        String filename = UtilityService.getCurrentTimeStamp().getTime() + "_" + UtilityService.generateOtp(6);
        String filePath = directory.concat(filename);

        Files.copy(new URL(s).openStream(), Paths.get(filePath));
        Mp3File mp3File = new Mp3File(filePath);

        long fileLength =  mp3File.getLengthInSeconds();

        Files.delete(Paths.get(filePath));

        return fileLength - 1;
    }

    public static boolean validImageFile(MultipartFile file) {

        String[] filePaths = file.getOriginalFilename().split("\\.");
        String fileExtension = filePaths[filePaths.length - 1];

        String[] validExtensions = new String[]{"JPG","JPEG","PNG","GIF"};
        List<String> imageExtensions = Arrays.asList(validExtensions);

        if( imageExtensions.contains(fileExtension.toUpperCase())){

            return true;
        }

        return false;
    }

    public static boolean validAudioFile(MultipartFile file) {

        String[] filePaths = file.getOriginalFilename().split("\\.");
        String fileExtension = filePaths[filePaths.length - 1];

        String[] validExtensions = new String[]{"MP3","MPEG","AAC","WMA"};
        List<String> audioExtensions = Arrays.asList(validExtensions);

        if( audioExtensions.contains(fileExtension.toUpperCase())){

            return true;
        }

        return false;
    }

    public static String toHex(String[] rgbArr){

        String hex = "#";

        for(int i = 0; i< 3; i++){

            int rgb = Integer.parseInt(rgbArr[i]);
            StringBuilder builder = new StringBuilder(Integer.toHexString(rgb & 0xff));
            while (builder.length() < 2) {
                builder.append("0");
            }
            hex = hex.concat(builder.toString().toUpperCase());
        }

        return hex;
    }

    public static Map getPaginatedResponse(Page pageObject, int page, int pageSize, List data){

        long totalRecords = pageObject.getTotalElements();
        long totalPages = pageObject.getTotalPages();
        boolean isLast = pageObject.isLast();
        boolean isFirst = pageObject.isFirst();

        Map responseBody = new HashMap();
        responseBody.put("totalRecords", totalRecords);
        responseBody.put("totalPages", totalPages);
        responseBody.put("page",page);
        responseBody.put("pageSize", pageSize);
        responseBody.put("isLast", isLast);
        responseBody.put("isFirst", isFirst);
        responseBody.put("content", data);

        return responseBody;
    }
}

