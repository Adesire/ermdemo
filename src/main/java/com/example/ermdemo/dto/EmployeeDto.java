package com.example.ermdemo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {
    private long id = 0;
    private String staffId;
    private String lastName;
    private String firstName;
    private String otherNames;
    private String mobileNo;
    private String phoneNo;
    private String email;
    private int isMale;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateOfBirth;
    private long departmentId;
    private String designation;
    private long levelId;
    private long lineManagerId;
    private boolean isLineManager;
    private boolean isDepartmentHead;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date employeeStartDate;
    private long roleId;
}
