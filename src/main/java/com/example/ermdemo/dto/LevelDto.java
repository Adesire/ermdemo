package com.example.ermdemo.dto;

import lombok.Data;

@Data
public class LevelDto {
    private String name;
    private int annualLeaveDays;
}
