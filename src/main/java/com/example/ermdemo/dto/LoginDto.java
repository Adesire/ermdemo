package com.example.ermdemo.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String googleIdToken;
}
