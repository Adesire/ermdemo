package com.example.ermdemo.dto;

import lombok.Data;

@Data
public class PermissionDto {
    private String name;

    public String getName() {
        return name.replace(" ", "_").trim().toUpperCase();
    }
}
