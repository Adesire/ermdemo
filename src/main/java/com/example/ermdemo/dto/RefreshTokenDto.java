package com.example.ermdemo.dto;

import lombok.Data;

@Data
public class RefreshTokenDto {

    private String refreshToken;

}
