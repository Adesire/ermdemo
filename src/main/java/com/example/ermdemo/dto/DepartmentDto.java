package com.example.ermdemo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DepartmentDto {
    @NotNull
    private String name;
    private Long head;
}
