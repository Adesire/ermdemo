package com.example.ermdemo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "employee")
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(unique = true, nullable = false)
    private long id;
    @Column(unique = true)
    private String staffId;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String firstName;

    private String otherNames;
    private String mobileNo;
    private String phoneNo;
    @Column(nullable = false)
    private String email;
    private int isMale;

    @JsonFormat(pattern="yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @OneToOne
    @JoinColumn(name = "department_id")
    @JsonBackReference
    private Department department;
    private String designation;
    @OneToOne
    @JoinColumn(name = "level_id")
    @JsonBackReference
    private Level level;
    @ManyToOne
    @JoinColumn(name = "lineManager_id")
    @JsonBackReference
    private LineManager lineManager;

//    @ManyToMany
//    @JoinTable(name = "emp_line_manager",
//            joinColumns = @JoinColumn(name = "employees_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "line_manager_id", referencedColumnName = "id"))
//    private LineManager lineManager;

    private boolean isLineManager = false;
    private boolean isDepartmentHead = false;

    @Temporal(TemporalType.DATE)
    private Date employeeStartDate;

//    @OneToMany
//    @JoinColumn(name = "leave_history_id")
//    private List<LeaveHistory> leaveHistoryList;

    private String imageUpload;

    //private List<String> otherDocs;

    private Timestamp lastLogin;

    @ManyToOne
    @JoinColumn(name = "roles_id")
    @JsonBackReference
    private Role role;

    private long createdBy;

    @Column(name = "is_active")
    private boolean active = false;

    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(name = "created_at",nullable = false,updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;

}

