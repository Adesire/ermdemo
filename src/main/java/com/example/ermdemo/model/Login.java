package com.example.ermdemo.model;

import lombok.Data;

@Data
public class Login {
    private String email;
    private String password;

    public Login() {
    }
}
