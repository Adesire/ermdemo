package com.example.ermdemo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "level")
public class Level {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private int annualLeaveDays;

}
