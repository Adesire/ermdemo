package com.example.ermdemo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "emp_leave")
public class Leave{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "leave_type_id")
    private LeaveType leaveType;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDateTime startDate;

    private int numberOfDays;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDateTime endDate;

    private int daysLeft;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    @JsonBackReference
    private Employee employeeId;

    private ApprovalStatus approvalStatus;
    private CompletedStatus completedStatus;

}
