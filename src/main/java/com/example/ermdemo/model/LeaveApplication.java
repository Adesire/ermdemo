//package com.example.ermdemo.model;
//
//import lombok.*;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import javax.persistence.*;
//import javax.validation.constraints.NotNull;
//import java.time.LocalDateTime;
//
//@Data
//@Entity
//@Table(name = "leave_application")
//public class LeaveApplication {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private int id;
//
//    @DateTimeFormat(pattern = "dd/MM/yyyy")
//    private LocalDateTime startDate;
//
//    @DateTimeFormat(pattern = "dd/MM/yyyy")
//    private LocalDateTime endDate;
//
//    private int workingDaysNum;
//
//    private int daysLeft;
//
//    private int currentDays;
//
//    @NotNull
//    @ManyToOne
//    @JoinColumn(name = "leave_id")
//    private Leave leave;
//
//    //@NotNull
//   /* @ManyToOne
//    @JoinColumn(name = "employeeId")*/
//    private int employeeId;
//
//}
