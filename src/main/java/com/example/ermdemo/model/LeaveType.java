package com.example.ermdemo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
//@Table(name = "leave_type")
public class LeaveType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
}
