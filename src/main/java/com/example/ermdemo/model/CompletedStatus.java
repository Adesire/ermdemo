package com.example.ermdemo.model;

public enum CompletedStatus {
    PENDING, STARTED, COMPLETED, CANCELED
}
